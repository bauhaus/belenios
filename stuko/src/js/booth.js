import '../scss/booth.scss'

function insertAfter(referenceNode, newNode) {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function showAlert(message) {
  const id = "alert-" + btoa(message);
  if (document.getElementById(id)) {
    return;
  }

  const alert = document.createElement("div");
  alert.setAttribute("id", id);
  alert.setAttribute("class", "alert alert--warning");
  alert.innerText = message;

  if (intro.style.display !== "none") {
    insertAfter(intro.querySelector(".current_step"), alert);
  } else {
    question_div.querySelector("div:nth-of-type(2)").prepend(alert);
  }
}

function removeAlerts() {
  Array.from(document.getElementsByClassName("alert"))
    .forEach(function(el) {
      el.remove();
    });
}

function setupCredentialInput() {
  const input = document.getElementById("credential_input");
  const label = input.parentElement.getElementsByTagName("span")[0];
  label.textContent = label.textContent.replace(/:$/, "");

  input.focus();

  window.prompt = function() {
    return input.value;
  }

  const button = document.getElementById("credential_submit")
  button.onclick = function() {
    document.querySelector("#input_code button").click();
  }

  window.alert = showAlert;
}

function setupQuestionButtons() {
  /* if (btn_prev) {
    const prev = btn_prev.onclick; 
    btn_prev.onclick = (e) => { removeAlerts(); prev(e); }
  } */

  if (btn_next) {
    const next = btn_next.onclick; 
    btn_next.onclick = (e) => { removeAlerts(); next(e); }
  }
}

function createTextfieldOutline() {
  const span = document.createElement("span")
  span.setAttribute("class", "mdc-notched-outline")

  const l = document.createElement("span");
  l.setAttribute("class", "mdc-notched-outline__leading");
  const n = document.createElement("span");
  n.setAttribute("class", "mdc-notched-outline__notch");
  const t = document.createElement("span");
  t.setAttribute("class", "mdc-notched-outline__trailing");

  span.append(l, n, t);

  return span;
}

function createTextfield() {
  const div = document.createElement("div");
  div.setAttribute("class", "mdc-text-field mdc-text-field--outlined");

  const outline = createTextfieldOutline();
  div.appendChild(outline)

  const input = document.createElement("input");
  input.setAttribute("class", "mdc-text-field__input");
  input.setAttribute("id", "credential");
  input.setAttribute("type", "text");

  div.appendChild(input);
  return div;
}

function updateElectionDescription() {
  const mutationObserver = new MutationObserver(() => {
    if (election_description.textContent.length > 0) {
      try {
        const parsed = JSON.parse(election_description.textContent);
    
        if (typeof parsed === "object") {
          election_description.textContent = parsed.description;
        }

        mutationObserver.disconnect();
      } catch(_) {}
    }
  });

  mutationObserver.observe(election_description, {
    attributes: false,
    characterData: false,
    childList: true,
    subtree: false,
    attributeOldValue: false,
    characterDataOldValue: false
  });
}

window.addEventListener("load",  function() {
  updateElectionDescription();

  const hash = Object.fromEntries(window.location.hash.substr(1).split("&").map(function(s) { return s.split("=") }));
  const uuid = hash.uuid;
  document.getElementsByTagName("img")[1].src = `./static/logos/${uuid}/left.png`
  document.getElementsByTagName("img")[2].src = `./static/logos/${uuid}/right.png`

  setupCredentialInput();

  var mutationObserver = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
      if (progress2.style["font-weight"] === "bold") {
          progress2.style["font-weight"] = "unset";
          progress2.classList.add("current");
          progress1.classList.remove("current");
          progress1.classList.add("done");
      }
      if (progress3.style["font-weight"] === "bold") {
        progress3.style["font-weight"] = "unset";
        progress3.classList.add("current");
        progress2.classList.remove("current");
        progress2.classList.add("done");
        progress1.classList.remove("current");
        progress1.classList.add("done");
      }
    });

    const answers = Array.from(question_div.querySelectorAll('input[type="checkbox"]'));
    for(const answer of answers) {
      console.log(answer)
      if (answer.parentElement.classList.contains("matter-checkbox")) {
        continue;
      }

      const matter = document.createElement("label");
      matter.setAttribute("class", "matter-checkbox");
      const matterLabel = document.createElement("span");
      matterLabel.textContent = answer.parentElement.textContent;
      matter.append(matterLabel);

      answer.parentNode.removeChild(answer.nextSibling);
      answer.parentNode.appendChild(matter);
      matter.prepend(answer);
    }
  });

  mutationObserver.observe(question_div, {
    attributes: false,
    characterData: false,
    childList: true,
    subtree: true,
    attributeOldValue: false,
    characterDataOldValue: false
  });
  mutationObserver.observe(plaintext_div, {
    attributes: false,
    characterData: false,
    childList: true,
    subtree: true,
    attributeOldValue: false,
    characterDataOldValue: false
  });
});