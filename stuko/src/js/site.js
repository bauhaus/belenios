import '../scss/site.scss'

function insertAfter(referenceNode, newNode) {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

window.addEventListener("load", () => {
  if(window.location.href.endsWith("/ballots")) {
    const title = document.getElementsByTagName("h1")[0];

    const description = document.createElement("p");
    description.setAttribute("id", "subtitle");
    description.textContent = title.textContent.split("—")[1].trim();

    insertAfter(title, description);
    title.textContent = title.textContent.split("—")[0].trim();

    title.style.opacity = 1;
    description.style.opacity = 1;
  }
});