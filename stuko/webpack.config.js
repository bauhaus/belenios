const path = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const cssOutput = "stuko.css";

module.exports = (env) => {
  return [
    {
      entry: {
        site: "./src/js/site.js",
        booth: "./src/js/booth.js"
      },
      output: {
        path: path.join(__dirname, "./dist"),
        filename: "[name].js"
      },
      plugins: [new MiniCssExtractPlugin({
        filename: "[name].css"
      })],
      module: {
        rules: [
          {
            test: /\.js$/,
            use: "babel-loader",
          },
          {
            test: /\.scss$/,
            use: [MiniCssExtractPlugin.loader, 'css-loader', "sass-loader"],
          },
        ],
      },
    },
  ];
};
