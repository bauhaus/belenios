<!-- -*- Mode: Xml -*- -->
<ocsigen>

  <server>

    <port>{{ getenv "HOST" "0.0.0.0" }}:{{ getenv "PORT" "8001" }}</port>

    <logdir>{{ getenv "LOGDIR" "/var/log/belenios" }}</logdir>
    <datadir>{{ getenv "VARDIR" "/var/belenios" }}/lib</datadir>

    <uploaddir>{{ getenv "VARDIR" "/var/belenios" }}/upload</uploaddir>

    <maxuploadfilesize>{{ getenv "MAXUPLOADFILESIZE" "5120kB" }}</maxuploadfilesize>
    <maxconnected>{{ getenv "MAXCONNECTED" "500" }}</maxconnected>

    <commandpipe>{{ getenv "RUNDIR" "/tmp/belenios" }}/ocsigenserver_command</commandpipe>

    <charset>utf-8</charset>

    <findlib path="{{ getenv "LIBDIR" "/usr/lib" }}"/>

    <extension findlib-package="ocsigenserver.ext.staticmod"/>
    <extension findlib-package="ocsigenserver.ext.redirectmod"/>

    <extension findlib-package="ocsigenserver.ext.ocsipersist-sqlite">
      <database file="{{ getenv "VARDIR" "/var/belenios" }}/lib/ocsidb"/>
    </extension>

    <extension findlib-package="eliom.server"/>
    <extension findlib-package="belenios-platform-native"/>

    <host charset="utf-8" hostfilter="*" defaulthostname="localhost">
      <site path="static" charset="utf-8">
        <static dir="{{ getenv "SHAREDIR" "/usr/share/belenios-server" }}" cache="0"/>
      </site>
      <site path="monitor">
        <eliom findlib-package="eliom.server.monitor.start"/>
      </site>
      <eliom findlib-package="belenios-server">


        <domain name="{{ getenv "DOMAIN" }}"/>
        <maxrequestbodysizeinmemory value="{{ getenv "MAXREQUESTBODYSIZEINMEMORY" "1048576" }}"/>
        <maxmailsatonce value="{{ getenv "MAXMAILSATONCE" "1000" }}"/>
        <uuid length="14"/>
        <gdpr uri="{{ getenv "GDPR_POLICY" }}"/>
        <contact uri="mailto:{{ getenv "CONTACT" }}"/>
        <server mail="{{ getenv "MAIL_FROM" }}" return-path="{{ getenv "MAIL_RETURN_PATH" }}"/>

        {{ if getenv "HTTPS" -}}
        <rewrite-prefix src="http://{{ getenv "DOMAIN" }}:{{ getenv "PORT" "8001" }}" dst="https://{{ getenv "DOMAIN" }}"/>
        {{- end }}

        {{ range $k, $v := .Env -}}
        {{ if ($k | regexp.Match `^AUTH_[a-zA-Z0-9]+_NAME$`) -}}
        {{- $key := ($k | strings.TrimSuffix "_NAME") -}}
        {{- $proto := (getenv (print $key "_PROTO")) -}}
        {{- if $proto -}}
        <auth{{ if getenv (print $key "_EXPORT") }}-export{{ end }} name="{{ $v }}">
          {{ if eq $proto "dummy" -}}
          <dummy/>
          {{- else if eq $proto "password" -}}
          <password db="{{ getenv (print $key "_DB") }}"/>
          {{- else if eq $proto "cas" -}}
          <cas server="{{ getenv (print $key "_SERVER") }}"/>
          {{- else if eq $proto "oidc" -}}
          <oidc server="{{ getenv (print $key "_SERVER") }}" client_id="{{ getenv (print $key "_CLIENTID") }}" client_secret="{{ getenv (print $key "_CLIENTSECRET") }}"/>
          {{- else if eq $proto "email" -}}
          <email/>
          {{- end }}
        </auth{{ if getenv (print $key "_EXPORT") }}-export{{ end }}>
        {{- else -}}
        <auth{{ if getenv (print $key "_EXPORT") }}-export{{ end }} name="{{ $v }}"/>
        {{- end }}
        {{- end }}
        {{- end }}

        <source file="{{ getenv "SHAREDIR" "/usr/share/belenios-server" }}/belenios.tar.gz"/>
        <default-group file="{{ getenv "SHAREDIR" "/usr/share/belenios-server" }}/groups/default.json"/>
        <nh-group file="{{ getenv "SHAREDIR" "/usr/share/belenios-server" }}/groups/rfc3526-2048.json"/>
        <log file="{{ getenv "LOGDIR" "/var/log/belenios" }}/security.log"/>
        <locales dir="{{ getenv "SHAREDIR" "/usr/share/belenios-server" }}/locales"/>
        <spool dir="{{ getenv "VARDIR" "/var/belenios" }}/spool"/>
        <warning file="{{ getenv "VARDIR" "/var/belenios" }}/warning.html"/>

      </eliom>
    </host>
  </server>
</ocsigen>
