import express, { Request, Response } from "express";
import fetch from "node-fetch";

const BELENIOS_SERVER = "http://belenios:8001";

const DEFAULT_LOGOS = {
  left: "https://belenios.loria.fr/static/logo.png",
  right: "https://belenios.loria.fr/static/placeholder.png"
}

const PASS_HEADERS = ["content-type", "content-length", "etag", "last-modified"]

function pick<T extends object, K extends keyof T>(obj: T, keys: K[]): Pick<T, K> {
  const entries = keys.map(key => ([key, obj[key]]));
  return Object.fromEntries(entries);
}

async function fetchElectionData(req: Request, uuid: string): Promise<any> {
  const res = await fetch(`${BELENIOS_SERVER}/elections/${uuid}/election.json`)
    .then(res => {
      if (res.ok) {
        return res;
      }

      return fetch(`${BELENIOS_SERVER}/draft/preview/${uuid}/election.json`, {
        headers: req.headers.cookie ? {
          cookie: req.headers.cookie
        } : undefined
      });
    });
  
  if (!res.ok) {
    return;
  }

  return res.json();
}

type LogoName = keyof typeof DEFAULT_LOGOS;

function isLogoName(name: string): name is LogoName {
  return Object.keys(DEFAULT_LOGOS).includes(name);
}

const app = express();

app.get("/:uuid/:logo.:extension", async (req: Request, res: Response) => {
  const logo = req.params.logo;
  if (!isLogoName(logo)) {
    return res.sendStatus(404);
  }

  const election = await fetchElectionData(req, req.params.uuid);
  if (!election) {
    return res.sendStatus(404);
  }
  
  let logoURL = DEFAULT_LOGOS[logo];

  try {
    const parsed = JSON.parse(election.description);
    
    if (parsed.logos && parsed.logos[logo]) {
      logoURL = parsed.logos[logo];
    }
  } catch {}

  fetch(logoURL).then((response) => {
    if(!response.ok) {
      res.sendStatus(404)
    } else {
      res.set({
        ...pick(Object.fromEntries(response.headers.entries()), PASS_HEADERS),
        "cache-control": [ "no-cache" ]
      });
      response.body.pipe(res)
    }
  });
});

app.listen(3000, "0.0.0.0");