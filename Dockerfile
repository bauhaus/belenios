FROM ubuntu:focal

ENV DEBIAN_FRONTEND noninteractive

ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
COPY --from=hairyhenderson/gomplate:v3.8.0 /gomplate /bin/gomplate

RUN apt-get update -qq && \
    apt-get install -y -qq bubblewrap build-essential libgmp-dev \
        libpcre3-dev pkg-config m4 libssl-dev libsqlite3-dev wget \
        ca-certificates zip unzip libncurses-dev zlib1g-dev \
        libgd-securityimage-perl cracklib-runtime jq git gosu \
        ssmtp mailutils && \
    adduser --disabled-login belenios 

USER belenios
WORKDIR /home/belenios
ARG CI_COMMIT_BRANCH=master
ARG CI_COMMIT_TAG
RUN git clone https://gitlab.inria.fr/belenios/belenios.git /tmp/belenios && \
    [ "$CI_COMMIT_BRANCH" != "master" ] && git -C /tmp/belenios checkout "$CI_COMMIT_BRANCH" ; \
    [ ! -z "$CI_COMMIT_TAG" ] && git -C /tmp/belenios reset --hard "$CI_COMMIT_TAG" ; \
    cp /tmp/belenios/.opamrc-nosandbox .opamrc && \
    cp /tmp/belenios/opam-bootstrap.sh opam-bootstrap.sh && \
    ./opam-bootstrap.sh && \
    cp -r /tmp/belenios/. /home/belenios && \
    rm -r /tmp/belenios

RUN eval $(cat ./env.sh) && \
    make build-release-server

USER root
RUN cp -r /home/belenios/_run/usr/* /usr
COPY templates/ /etc/default/belenios/
COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/tini", "--", "/entrypoint.sh"]
CMD ["belenios"]