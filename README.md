# Belenios

Docker images for Belenios a verifiable online voting system http://www.belenios.org

## Configuration

Belenios can be customized using the following environment variables.

| Key | Value | Default |
| --- |  ---  |   ---   |
| HOST | `ip` | "0.0.0.0" |
| PORT | `integer` | 8001 |
| DOMAIN | `domain` | - |
| MAIL_HOST | `domain` | - |
| MAIL_PORT | `integer` | 587 |
| MAIL_USER | `email` | - |  |
| MAIL_PASSWORD | `string` | - |
| MAIL_FROM | `email` | - |
| MAIL_RETURN_PATH | `email` | - |
| MAIL_TLS | "YES" \| "NO" | "NO" |
| MAIL_STARTTLS | "YES" \| "NO" | "NO" |
| CONTACT | `email` | - |
| GDPR_POLICY | `url` | - |
| AUTH_[a-zA-Z0-9]_PROTO | dummy \| password \| cas \| oidc | - |
| AUTH_[a-zA-Z0-9]_NAME | `string` | env(AUTH_[a-zA-Z0-9]_PROTO) |
| AUTH_[a-zA-Z0-9]_SERVER | `url` | - |
| AUTH_[a-zA-Z0-9]_CLIENTID | `string` | - |
| AUTH_[a-zA-Z0-9]_CLIENTSECRET | `string` | - |
| AUTH_[a-zA-Z0-9]_EXPORT | `string` | - |

## Development

```bash
[belenios]$ docker-compose -f docker-compose.dev.yml up

# CSS and JS frontend
[belenios/stuko]$ npm ci
[belenios/stuko]$ npm run watch
```