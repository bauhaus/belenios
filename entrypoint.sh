#!/bin/bash

set -e

# contents of /home/belenios/env.sh
PATH="/home/belenios/.belenios/bootstrap/bin:$PATH"; export PATH;
OPAMROOT=/home/belenios/.belenios/opam; export OPAMROOT;
XDG_CACHE_HOME=/home/belenios/.belenios/cache; export XDG_CACHE_HOME;
eval $(gosu belenios opam env)

: ${BELENIOS_CONFIG_TEMPLATE:=/etc/default/belenios/ocsigenserver.conf.tmpl}
: ${BELENIOS_CONFIG:=/etc/belenios/ocsigenserver.conf}
: ${BELENIOS_LOGDIR:=/var/log/belenios}
: ${BELENIOS_VARDIR:=/var/belenios}
: ${BELENIOS_RUNDIR:=/tmp/belenios}
: ${BELENIOS_LIBDIR:=/usr/lib}
: ${BELENIOS_SHAREDIR:=/usr/share/belenios-server}
: ${BELENIOS_CONFIGDIR:=/etc/belenios}

mkdir -p \
    $BELENIOS_LOGDIR \
    $BELENIOS_VARDIR/lib \
    $BELENIOS_VARDIR/upload \
    $BELENIOS_VARDIR/spool \
    $BELENIOS_RUNDIR \
    $BELENIOS_CONFIGDIR

chown -R belenios:belenios $BELENIOS_VARDIR $BELENIOS_LOGDIR $BELENIOS_RUNDIR

find $BELENIOS_VARDIR -type d -exec chmod 700 {} \;
find $BELENIOS_VARDIR -type f -exec chmod 600 {} \;

chmod o+w /dev/stdout
chmod o+w /dev/stderr
ln -sf /dev/stdout $BELENIOS_LOGDIR/access.log
ln -sf /dev/stderr $BELENIOS_LOGDIR/errors.log
ln -sf /dev/stdout $BELENIOS_LOGDIR/security.log
ln -sf /dev/stderr $BELENIOS_LOGDIR/warnings.log

gomplate -f "$BELENIOS_CONFIG_TEMPLATE" > "$BELENIOS_CONFIG"
chown root:belenios "$BELENIOS_CONFIG"
chmod 640 "$BELENIOS_CONFIG"

gomplate -f "/etc/default/belenios/ssmtp.conf.tmpl" > "/etc/ssmtp/ssmtp.conf"
gomplate -f "/etc/default/belenios/revaliases.tmpl" > "/etc/ssmtp/revaliases"

export HOME=${BELENIOS_VARDIR}

if [[ "$1" == "belenios" ]]; then
    shift
    exec gosu belenios:belenios ocsigenserver -c $BELENIOS_CONFIG "$@"
else
    exec $@
fi