#!/usr/bin/env python3

import argparse
import json
import requests
import sys
import traceback

parser = argparse.ArgumentParser(description='Tally the election')
parser.add_argument('uuid', metavar='UUID', help='The uuid of the election you want to tally.')

SERVER = "https://wahl.m18.uni-weimar.de"

def fetch_election(server, uuid):
  response = requests.get(server + "/elections/" + uuid + "/election.json")
  return json.loads(response.text)

def fetch_result(server, uuid):
  response = requests.get(server + "/elections/" + uuid + "/result.json")
  return json.loads(response.text)

def get_election_mode(election):
  description = json.loads(election.get("description"))
  return description.get("mode")

def main():
  args = parser.parse_args()

  # fetch election from server
  try:
    election = fetch_election(SERVER, args.uuid)
  except json.decoder.JSONDecodeError:
    print("Fetching the election result failed. This may happen for elections that have not yet been open or have been deleted.")
    sys.exit(1)

  if get_election_mode(election) != "multivote":
    print("The election with uuid " + args.uuid + " is not supported by this tool.")
    sys.exit(1)

  # fetch results from server
  try:
    results = fetch_result(SERVER, args.uuid)
  except json.decoder.JSONDecodeError:
    print("Fetching the election result failed. This may happen for elections that have not yet been tallied.")
    sys.exit(1)
  
  num_tallied = results.get("num_tallied")
  result = results.get("result")

  print("Election:        " + election.get("name"))
  print("UUID:            " + election.get("uuid"))
  print("Administrator:   " + election.get("administrator"))
  print("Tallied ballots: " + str(num_tallied))

  print ()

  question = election.get("questions")[0]
  answers = question.get("answers")
  num_answers = len(answers)

  # tally the election
  tallied_result = [0] * num_answers
  for r in result:
    # if blank vote is allowed the first index indicates if the vote is blank
    offset = 0
    if question.get('blank') == True:
      offset = 1

      # if the vote is blank we don't count it
      if r[0] == 1:
        continue

    for i in range(0, num_answers):
      tallied_result[i] += r[i + offset]

  # sort and print tallied result
  answers_with_result = []
  for i in range(0, num_answers):
    answers_with_result.append({
      'answer': answers[i],
      'result': tallied_result[i]
    })
  answers_with_result.sort(key=lambda a: a.get('result'), reverse=True)

  for answer in answers_with_result:
    print(str(answer.get('result')).ljust(5) + " " + answer.get('answer'))


if __name__ == "__main__":
  main()