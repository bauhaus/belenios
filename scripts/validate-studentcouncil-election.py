#!/usr/bin/env python3

import argparse
import json
import sys

BLANK_REQUIRED = True
MIN_VOTES = 1
MAX_VOTES = 1

parser = argparse.ArgumentParser(description='Validate a studentcouncil election at the Bauhaus University in Weimar')
parser.add_argument('election', metavar='ELECTION', help='Path to the election.json file.')

def print_error(message):
  print("[ERROR]: {}".format(message))

# https://wahl.m18.uni-weimar.de/draft/preview/<UUID>/election.json
def main():
  args = parser.parse_args()

  with open(args.election) as f:
    election = json.load(f)

  # VALIDATE DESCRIPTION
  try:
    description = json.loads(election.get("description"))

    # check if multivote mode is set
    if description.get("mode") != "multivote":
      print_error("description.mode is not multivote")

    # check if description is set
    if not description.get("description"):
      print_error("description.description is missing")

    # check if logos are set
    logos = description.get("logos")
    if not logos:
      print_error("description.logos is missing")

    if not logos.get("left"):
      print_error("description.logos.left is missing")

    if not logos.get("right"):
      print_error("description.logos.right is missing")
  except:
    print_error("Description has invalid JSON format.")


  # VALIDATE QUESTIONS
  questions = election.get("questions")

  for question in questions:
    # check if blank votes are allowed
    if BLANK_REQUIRED and question.get("blank") != True:
      print_error("Question '" + question.get("question") + "' does not allow blank votes.")
    
    # check if min is set correctly
    if MIN_VOTES != question.get("min"):
      print_error("Question '{}' sets {} as vote minimum but {} is required.".format(
        question.get("question"),
        question.get("min"),
        MIN_VOTES
      ))

    # check if max is set correctly
    if MAX_VOTES != question.get("max"):
      print_error("Question '{}' sets {} as vote maximum but {} is required.".format(
        question.get("question"),
        question.get("max"),
        MAX_VOTES
      ))

  # check that answers are identical
  for q in range(1, len(questions)):
    prev = questions[q - 1]
    cur = questions[q]

    if prev.get("answers") != cur.get("answers"):
      print_error("Answers of questions '" + prev.get("question") + "' and '" + cur.get("question") + "' differ.")

if __name__ == "__main__":
  main()